import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import CourseInterface from '../models/course.model';
import {CoursesService} from '../courses.service';
import AuthService from '../../auth/auth.service';
import {Router} from '@angular/router';
import RatingInterface from '../models/rating.model';

@Component({
  selector: 'app-course-card',
  templateUrl: './course-card.component.html',
  styleUrls: ['./course-card.component.css']
})
export class CourseCardComponent implements OnInit {

  @Input() course: CourseInterface;
  @Output() onDelete = new EventEmitter();

  isLoggedIn = false;
  isAdmin = false;

  constructor(private coursesService: CoursesService,
              private authService: AuthService,
              private router: Router) {

    this.isLoggedIn = this.authService.isLoggedIn();
    this.isAdmin = this.isLoggedIn && this.authService.getLoggedUser().isAdmin;
  }

  ngOnInit() {
  }

  onRateClick(ratingNum: number) {
    const userId = this.authService.getLoggedUser().id;
    const index = this.course.ratings.findIndex(u => u.userId === userId);
    if (index !== -1) {
      this.course.ratings[index].rating = ratingNum;
    } else {
      const rating: RatingInterface = {
        userId,
        courseId: this.course.id,
        rating: ratingNum
      };

      this.course.ratings.push(rating);
    }

    this.coursesService.rateCourse(this.course).subscribe(() => console.log('course rated'));
  }

  onDeleteClick(): void {
    this.onDelete.emit(this.course.id);
  }

  get rating(): number {
    return this.course.ratings.length === 0
      ? 0
      : this.course.ratings.map(r => r.rating).reduce((p, c, _) => p + c) / this.course.ratings.length;
  }
}
