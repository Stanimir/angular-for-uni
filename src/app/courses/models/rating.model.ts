export default interface RatingInterface {
  userId: number;
  courseId: string;
  rating: number;
}
