import RatingInterface from './rating.model';

export default interface CourseInterface {
  id?: string;
  title: string;
  description?: string;
  ratings?: RatingInterface[];
}
