import {Component, OnInit} from '@angular/core';
import CourseInterface from '../models/course.model';
import {CoursesService} from '../courses.service';
import {Router} from '@angular/router';
import AuthService from 'src/app/auth/auth.service';

@Component({
  selector: 'app-courses-list',
  templateUrl: './courses-list.component.html',
  styleUrls: ['./courses-list.component.css']
})
export class CoursesListComponent implements OnInit {

  courses: CourseInterface[] = [];

  isAdmin = false;

  constructor(private coursesService: CoursesService,
              private router: Router,
              private authService: AuthService) {
    this.isAdmin = authService.isLoggedIn() && authService.getLoggedUser().isAdmin;
  }

  ngOnInit() {
    this.coursesService.getAllCourses().subscribe((courses) => {
      console.log(courses);
      this.courses = courses;
    });
  }

  onCourseDeleted(id: string): void {
    this.coursesService.deleteCourse(id).subscribe(() => {
      this.courses = this.courses.filter(t => t.id !== id);
    });
  }

  onCourseAdd(): void {
    this.router.navigateByUrl('courses/add-course');
  }

}
