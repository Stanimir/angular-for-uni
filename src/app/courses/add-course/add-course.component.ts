import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '../../../../node_modules/@angular/forms';
import {CoursesService} from '../courses.service';
import {ActivatedRoute, Router} from '../../../../node_modules/@angular/router';
import CourseInterface from '../models/course.model';

@Component({
  selector: 'app-add-course',
  templateUrl: './add-course.component.html',
  styleUrls: ['./add-course.component.css']
})
export class AddCourseComponent implements OnInit {

  courseForm: FormGroup;

  constructor(private fb: FormBuilder,
              private coursesService: CoursesService,
              private router: Router,
              private route: ActivatedRoute) {

    this.route.params.subscribe((params) => {
      console.log(params);

      if (params.id) {
        this.coursesService.getById(params.id)
          .subscribe((course) => {
            this.createForm();

            this.courseForm.patchValue({...course});
          });
      }
    });
    this.createForm();
  }

  ngOnInit() {
  }

  private createForm(): void {
    this.courseForm = this.fb.group({
      id: [''],
      title: ['', Validators.required],
      description: ['', [Validators.required, Validators.minLength(3)]],
      creationDate: [''],
      assignees: ['']
    });
  }

  onFormSubmit(event): void {
    const newCourse = {...this.courseForm.value} as CourseInterface;
    newCourse.ratings = newCourse.ratings || [];
    this.coursesService.addCourse(newCourse)
      .subscribe(() => {
        this.router.navigateByUrl('courses/list');
      });
  }

  get isFormValid(): boolean {
    return this.courseForm.valid;
  }

  get title() {
    return this.courseForm.get('title');
  }

  get description() {
    return this.courseForm.get('description');
  }

}
