export default interface User {
  id: number;
  name: string;
  username: string;
  password: string;
  isAdmin?: boolean;
  isBlocked?: boolean;
}
