import {Component, OnInit} from '@angular/core';
import AuthService from 'src/app/auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLoggedIn = false;
  isAdmin = false;

  constructor(private authService: AuthService) {
    this.isLoggedIn = authService.isLoggedIn();
    this.isAdmin = this.isLoggedIn && authService.getLoggedUser().isAdmin;
  }

  ngOnInit() {
  }

  logout() {
    this.authService.logout();
  }
}
