import {Component, OnInit} from '@angular/core';
import AuthService from '../auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  isWrong: boolean;

  constructor(private authService: AuthService,
              private fb: FormBuilder,
              private router: Router) {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]

    });
    this.isWrong = false;
  }

  ngOnInit() {
  }

  login(usernameField, passwordField): void {
    this.authService
      .login(usernameField.value, passwordField.value)
      .subscribe(() => {
        this.router.navigateByUrl('users/list');
      }, (error) => {
        console.error(error);
      });
  }

}
