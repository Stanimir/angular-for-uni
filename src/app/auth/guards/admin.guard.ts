import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivateChild, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment} from '@angular/router';
import {Observable} from 'rxjs';
import AuthService from '../auth.service';
import {CanActivate} from '@angular/router/src/utils/preactivation';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanLoad, CanActivate, CanActivateChild {
  path: ActivatedRouteSnapshot[];
  route: ActivatedRouteSnapshot;

  constructor(private authService: AuthService,
              private router: Router) {
  }

  canLoad(route: Route,
          segments: UrlSegment[])
    : boolean | Observable<boolean> | Promise<boolean> {
    if (!this.authService.isLoggedIn() || !this.authService.getLoggedUser().isAdmin) {
      this.router.navigateByUrl('auth/login');
    }

    return true;
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {

    if (!this.authService.isLoggedIn()) {
      this.router.navigateByUrl('auth/login');
      return false;
    } else if (!this.authService.getLoggedUser().isAdmin) {
      this.router.navigateByUrl('courses/list');
      return false;
    }

    return true;
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    return this.canActivate(route, state);
  }
}
